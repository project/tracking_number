Tracking number
Copyright 2014 Chris Caldwell <chrisolof@gmail.com>

Description
-----------
This module provides a field for storage and display of shipment tracking
numbers.

Features
--------
* Store tracking numbers for:
  - United States Postal Service
  - UPS
  - FedEx
  - DHL
  - DHL Global
  - Others via hook_tracking_number_organizations_alter()
* Display your tracking numbers as either:
  - A link to track the package on the shipper's external website.
  - A plain-text representation of the tracking number and shipping
    organization.

Installation
------------
To install, place this module into your site's modules folder and enable the
"Tracking number" module, which can be found under "Fields" on the admin/modules
page.
-or-
drush dl tracking_number
drush en tracking_number

Configuration
-------------
To use this module, add and configure a field of type "Tracking number" to an
entity of your choosing.  Under the entity's display settings, configure the
field display to either print out as plain text or as a link to allow viewers to
track their shipment.

Bugs, Features, & Patches
-------------------------
If you wish to report bugs, add feature requests, or submit patches, you can do
so on the project page on Drupal.org.
http://drupal.org/project/tracking_number

Todo
----
* Add out-of-the-box support for more shipping companies/organizations
* Allow for greater customization of the field output
* Add better token support
* Add functional tests (SimpleTest)

Author
------
Chris Caldwell (http://www.lima-solutions.net) <chrisolof@gmail.com>

The author can be contacted for paid customizations to this and other modules.
