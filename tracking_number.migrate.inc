<?php

/**
 * @file
 * Support for migrate module.
 *
 * With Migrate 2.4 or later, you can use the subfield syntax to set the
 * tracking number and organization:
 * @code
 * $this->addFieldMapping('field_tracking', 'source_tracking_number');
 * $this->addFieldMapping('field_tracking:organization', 'source_organization');
 * @endcode
 *
 * With earlier versions of Migrate, you must pass an arguments array:
 * @code
 * $tracking_args = array(
 *   'organization' => array('source_field' => 'source_organization'),
 * );
 * $this->addFieldMapping('field_my_tracking', 'source_tracking_number')
 *      ->arguments($tracking_args);
 * @endcode
 */

/**
 * Implements hook_migrate_api().
 */
function tracking_number_migrate_api() {
  return array(
    'api' => 2,
    'field handlers' => array('TrackingNumberFieldHandler'),
  );
}

/**
 * Provides migration support for tracking number fields.
 */
class TrackingNumberFieldHandler extends MigrateFieldHandler {
  /**
   * Construct the tracking number field handler.
   */
  public function __construct() {
    $this->registerTypes(array('tracking_number_field'));
  }

  /**
   * Define the available arguments.
   *
   * @param string $organization
   *   The shipping organization this tracking number belongs to.
   *
   * @return array
   *   An array of argument values.
   *
   * @see tracking_number_organizations()
   */
  public static function arguments($organization = NULL) {
    $arguments = array();
    if (!is_null($organization)) {
      $arguments['organization'] = $organization;
    }
    return $arguments;
  }

  /**
   * Implementation of MigrateFieldHandler::fields().
   *
   * @param string $type
   *   The field type.
   * @param array $instance
   *   Instance info for the field.
   * @param object $migration
   *   The migration context for the parent field. We can look at the mappings
   *   and determine which subfields are relevant.
   *
   * @return array
   *   An array of the available subfields.
   */
  public function fields($type, array $instance, $migration = NULL) {
    return array(
      'organization' => t('Subfield: The shipping organization associated with this tracking number'),
    );
  }

  /**
   * Implementation of MigrateFieldHandler::prepare().
   *
   * Prepare tracking number data for saving as a tracking number field.
   *
   * @param object $entity
   *   The entity to which the field data will be attached.
   * @param array $field_info
   *   Information about the field.
   * @param array $instance
   *   The field instance.
   * @param array $values
   *   The values being passed into the field.
   *
   * @return array
   *   Field API array suitable for inserting in the destination object.
   */
  public function prepare($entity, array $field_info, array $instance, array $values) {
    if (isset($values['arguments'])) {
      $arguments = $values['arguments'];
      unset($values['arguments']);
    }
    else {
      $arguments = array();
    }

    $language = $this->getFieldLanguage($entity, $field_info, $arguments);
    $values = array_filter($values);

    foreach ($values as $delta => $value) {
      $item = array();
      if (isset($arguments['organization'])) {
        if (!is_array($arguments['organization'])) {
          $item['organization'] = $arguments['organization'];
        }
        elseif (isset($arguments['organization'][$delta])) {
          $item['organization'] = $arguments['organization'][$delta];
        }
      }
      $item['number'] = $value;
      $return[$language][$delta] = $item;
    }

    return isset($return) ? $return : NULL;
  }
}
