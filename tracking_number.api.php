<?php

/**
 * @file
 * Hooks provided by the Tracking number module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Modify tracking number shipping organizations.
 *
 * By implementing this hook, modules are able to add, customize, or remove
 * shipping organizations.  All defined organizations are available for
 * association with individual tracking numbers in tracking number fields,
 * and allow tracking numbers to display as links to track the shipments they
 * identify over on the shipping organization's external website.
 * The following properties can be defined for each organization:
 * - label: The human-readable display name of the organization.
 * - url: An associative array defining the URL to be used in tracking a
 *   shipment on the shipping organization's external website, with its parts
 *   suitable for use in l() or url().  See url() for more information.
 *   The following properties can be defined for each url:
 *   - path: The external path to the shipping organization's package tracking
 *     service, such as "http://example.com/package-tracking".
 *   - options: An associative array of additional options necessary to allow
 *     for tracking the package by way of the tracking number.  See url() for
 *     all available options.
 *
 * @param array $organizations
 *   An array containing the available shipping organizations, keyed by
 *   organization id.
 * @param string $number
 *   The tracking number to be substituted into the organization's shipment
 *   tracking URL so that tracking data for this specific shipment can be linked
 *   to.
 *
 * @see tracking_number_organizations()
 * @see tracking_number_field_formatter_view()
 */
function hook_tracking_number_organizations_alter(array &$organizations, &$number) {
  // Change the label for the United States Postal Service shipping
  // organization.
  $organizations['usps']['label'] = t('USPS');

  // Remove the UPS shipping organization.
  unset($organizations['ups']);

  // Add an additional shipping organization.
  $organizations['prime-air'] = array(
    'label' => t('Prime Air'),
    'url' => array(
      'path' => 'https://example.com/unmanned-aerial-shipments/track-by-number',
      'options' => array(
        'query' => array(
          'tracking_number' => $number,
        ),
      ),
    ),
  );
}

/**
 * @} End of "addtogroup hooks".
 */
